/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.testeCaraouCora;

import br.com.senac.impraoupar.ImparouPar;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author sala304b
 */
public class CaraCaroa {
    
    public CaraCaroa() {
    }
    
    @Test
    public void DeveRetornaPAR(){
        ImparouPar situacao = new ImparouPar();
        int numero = 2;
        boolean resultado = situacao.ePAR(numero);
        
        assertTrue (resultado);
        
    }
    
    
}
